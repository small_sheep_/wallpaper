#include "wallpaper/wallpaper/wallpaper.h"
#include "wallpaper/macro/macro.h"

#if defined SMALL_LINUX
#elif defined SMALL_WINDOWS
# include <windows.h>
#endif

Wallpaper::Wallpaper(char const *const local_wallpaper_path)
    : local_wallpaper_path_(local_wallpaper_path) {}

int Wallpaper::Use() {
  int result = 0; // `result' is 0 for function success.

  result = SystemSet();
  return result;
}

int Wallpaper::SystemSet() const {
  int result = 0;
#if defined SMALL_LINUX
  result = 1;
#elif defined SMALL_WINDOWS
  // If SystemParametersInfoA succeeds, it returns true
  result = ~SystemParametersInfoA(SPI_SETDESKWALLPAPER, 0,
                                  (void *)local_wallpaper_path_.CStr(),
                                  SPIF_UPDATEINIFILE);
#endif
  return result;
}



