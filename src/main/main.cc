#include "wallpaper/main/main.h"
#include <exception>
#include <iostream>
#include <filesystem>
#include <memory>
#include <stdexcept>
#include <string>
#include "boost/property_tree/json_parser.hpp"
#include "curl/curl.h"
#include "small_utility/file/file.h"
#include "small_utility/string/string.h"
#include "wallpaper/wallpaper/wallpaper.h"

using small_utility::string_stuff::String;

void FetchDataByURL(char const *const &url,
                    char const *const &output_file_path) {
  CURLcode curl_error_code;
  CURL *curl = curl_easy_init();
  if (!curl) {
    throw std::runtime_error("Failed to initialize 'curl_easy'.");
  }
  auto fclose_lambda = [](FILE *stream) {
    if (stream) {
      fclose(stream);
    }
  };
  // This is the file that will be written.
  std::unique_ptr<FILE, decltype(fclose_lambda)>
      stream(fopen(output_file_path, "wb"), fclose_lambda);
  auto WriteFunction = +[](void *const ptr, int const size, int const nmemb,
                           FILE *stream) {
    int written_size = fwrite(ptr, size, nmemb, stream);
    return written_size;
  };

  CURLcode &cec = curl_error_code;
  cec = curl_easy_setopt(curl, CURLOPT_URL, url);
  cec = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteFunction);
  cec = curl_easy_setopt(curl, CURLOPT_WRITEDATA, stream.get());
  cec = curl_easy_perform(curl);
  if (cec != CURLE_OK) {
    small_utility::string_stuff::String error_str("curl:");
    error_str += curl_easy_strerror(cec);
    throw std::runtime_error(error_str.CStr());
  }
  curl_easy_cleanup(curl);
}

int main() {
  namespace fs = std::filesystem;
  namespace pt = boost::property_tree;

  constexpr char const *temporary_directory_path = "./tmp/";
  constexpr char const *temporary_wallpaper_file_path = "./tmp/wallpaper.tmp";
  constexpr char const *weather_fetching_api =
      "http://v0.yiketianqi.com/api?unescape=1&version=v91&"
      "appid=43656176&appsecret=I42og6Lm&ext=&cityid=&city=";
  constexpr char const *temporary_weather_file_path = "./tmp/weather.tmp";

  try {
    std::cout << "Welcome to this application which is for setting desktop "
                 "backgrounds.\n";
    String wallpaper_path;
    std::cout << "Please enter the path of the wallpaper you want to use:\n";
    std::cin >> wallpaper_path;
    //wallpaper_path = "https://www.baidu.com/";
    //wallpaper_path = "/home/small-sheep/resource/image/lenovowallpaper.jpg";

    // The wallpaper will be stored in temporary_file_path.

    small_utility::file_stuff::MakeDirectory(temporary_directory_path);

    if (curl_global_init(CURL_GLOBAL_ALL) != CURLE_OK) {
      throw std::runtime_error("curl_global_initialization failed.");
    }

    bool source_is_web = wallpaper_path.Size() > 3 &&
        wallpaper_path.SubStringLength(0, 4) == "http";
    if (source_is_web) {
      // Fetch from web.
      FetchDataByURL(wallpaper_path.CStr(), temporary_wallpaper_file_path);
    } else {
      // Fetch from local.
      fs::copy(wallpaper_path.CStr(), temporary_wallpaper_file_path,
          fs::copy_options::overwrite_existing);
    }
    FetchDataByURL(weather_fetching_api, temporary_weather_file_path);

    curl_global_cleanup();

    pt::ptree tree;

    pt::json_parser::read_json(temporary_weather_file_path, tree);
    std::string weather = tree.get_child("data..wea").get_value<std::string>();
    std::cout << "The weather is " << weather << "\n";

    Wallpaper wallpaper(
        fs::absolute(temporary_wallpaper_file_path).string().c_str());
    std::cout << "Result of this application: " << wallpaper.Use() << ".\n";
  } catch (std::exception &e) {
    std::cerr << "An error occurred:\n"
                 "  e.what():  " << e.what() << "\n";
    throw e;
  }

  return 0;
}
