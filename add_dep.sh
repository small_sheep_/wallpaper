#!/bin/bash
if [[ ! -d 'deps' ]]
then
	mkdir deps
fi

# process arguments

while [[ -n ${1} ]]
do
	case ${1} in
		-url)
			shift
			case ${1} in
				*git*)
					from="git"
					;;
				*)
					from="web"
					;;
			esac
			url=${1}
			;;
		-name)
			shift
			name=${1}
			;;
		*)
			;;
	esac
	shift
done

# check arguments
if [[ -z ${name} ]]
then
	printf "Please add argument \'-name\'.\n"
	exit
elif [[ -z ${url} ]]
then
	printf "Please add argument \'-url\'.\n"
	exit
fi

# confirm & perform
printf "url=${url} name=${name} from=${from}\n"
printf "Do you want to continue to add \'${name}\'? [Y/n] "
read result
if [[ ${result} == [Yy] ]]
then
	if [[ ${from} == "git" ]]
	then
		git clone ${url} deps/${name}
		rm -rf deps/${name}/.git
	elif [[ ${from} == "web" ]]
	then
		curl ${url} -o deps/${name}
	fi
	sed -i -e "1i\add_subdirectory(${name})" deps/CMakeLists.txt
	sed -i -e "/set(libs/a\\\t${name}" src/main/CMakeLists.txt
fi
