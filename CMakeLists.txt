cmake_minimum_required(VERSION 3.18.4)

project(wallpaper VERSION 0.1.0)

#set(CMAKE_BUILD_TYPE Release)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-deprecated-copy -Werror -Wfatal-errors")

option(BUILD_SHARED_LIBS "Build shared libraries" OFF)
option(BUILD_CURL_EXE "Set to ON to build curl executable." OFF)
option(USE_MANUAL OFF)
option(BUILD_TESTING OFF)
option(SMALL_UTILITY_BUILD_TEST "build test executables." OFF)

#if(${CMAKE_HOST_UNIX})
#	if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
#		set(curl curl)
#	elseif()
#		set(curl curl)
#	endif()
#elseif(${CMAKE_HOST_WIN32})
#	if(${CMAKE_BUILD_TYPE} STREQUAL Debug)
#		set(curl curl)
#	else()
#		set(curl curl)
#	endif()
#endif()

add_subdirectory(deps)
add_subdirectory(src)
