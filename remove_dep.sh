#!/bin/bash

name=$1

printf "Are you sure to remove ${name}? [Y/n] "
read result
if [[ ${result} = [Yy] ]]
then
	rm -rf deps/${name}/
	sed -i -e "/\<${name}\>/d" deps/CMakeLists.txt
	sed -i -e "/set(libs/,/)/ {/\<${name}\>/d}" src/main/CMakeLists.txt
	printf "${name} was successfully removed!\n"
fi
