#include "small_utility/utility/utility.h"
#include <stdexcept>

namespace small_utility {

namespace utility_stuff {

void Sleep(int const time_millsecond) {
  if (time_millsecond < 0) {
    throw std::invalid_argument("time_millisecond should be in the range 0 to"
                                "999999999");
  }
#if defined SMALL_LINUX
  timespec ts;
  ts.tv_sec = time_millsecond / 1000;
  ts.tv_nsec = (time_millsecond % 1000) * 1000 * 1000;
  int const error = nanosleep(&ts, nullptr);
  if (error == -1) {
    throw std::runtime_error("Sleeping has been interrupted");
    //TODO(small_sheep_ 1178550325@qq.com): Please finish this throwing.
  }
#elif defined SMALL_WINDOWS
  ::Sleep(time);
#endif
}

FILE *Popen(char const *const command, char const *const type) {
  FILE *result;
#if defined SMALL_LINUX
  result = popen(command, type);
#elif defined SMALL_WINDOWS
  result = _popen(command, type);
#endif
  return result;
}

int Pclose(FILE *const file_ptr) {
  int result;
#if defined SMALL_LINUX
  result = pclose(file_ptr);
#elif defined SMALL_WINDOWS
  result = _pclose(file_ptr);
#endif
  return result;
}

} // namespace utility_stuff

} // namespace small_utility
