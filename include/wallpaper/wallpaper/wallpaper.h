#ifndef WALLPAPER_WALLPAPER_H_
#define WALLPAPER_WALLPAPER_H_

#include "small_utility/string/string.h"

using small_utility::string_stuff::String;

class Wallpaper {
 public:
  Wallpaper(char const *const local_wallpaper_path);
  int Use();
  int SystemSet() const;

 private:
  String local_wallpaper_path_;
};

#endif  // WALLPAPER_WALLPAPER_H_
